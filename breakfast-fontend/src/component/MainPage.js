import React from 'react';
import Top3 from './Top3';
import PieChart from './PieChart';
import CohortTable from './CohortTable'

class MainPage extends React.Component {
  constructor(props) {
    
    super(props);
    const { top3 ,Shippings ,noShippings,cohort} =this.props.data;
    this.state = {
      Shippings: Shippings,
      noShippings: noShippings,
      cohort: cohort,
      top3 : top3
    };
  }

  componentDidMount() {
  
   
  }
  

  render() {
    const { top3 ,Shippings ,noShippings,cohort} =this.state;
   return (
        <div>
            <h1>ANS</h1>
            <PieChart Shippings = {Shippings} noShippings = {noShippings} ></PieChart>
            <CohortTable cohort = {JSON.parse(cohort)}></CohortTable>
            <Top3 top3 ={top3}></Top3>
        </div>
   )
}

}
export default MainPage;