import React from 'react';


class Top3 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rsobj: {},
      isLoaded: false,
      error: null,
    };
  }

  componentDidMount() {
  
   
  }
  

  render() {
    const { top3 } =this.props;

    const Top3List = top3.map(value =>{
      return <div>{value}</div>
    })
   return ( 
      <div>
        <h2>Top3 List</h2>
        {Top3List}
      </div>
   )
}

}
export default Top3;