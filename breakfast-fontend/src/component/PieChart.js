import React from 'react';
import Chart from 'react-google-charts';

const style = {
  textAlign : 'center' ,
  width : '500px',
  margin: 'auto'

};

class PieChart extends React.Component {
    render() {
      const { Shippings ,noShippings, } =this.props;
    return(
      <div style={style} >
        <h2>Shpping Pie Chart</h2>
        <Chart 
            // width={}
            height={'300px'}
            chartType="PieChart"
            loader={<div>Loading Chart</div>}
            data={[
              ['Task', ''],
              ['Shppings', Shippings -noShippings],
              ['No Shpping', noShippings],
            ]}
            options={{
              title: 'Shpping Pie Chart',
            }}
            rootProps={{ 'data-testid': '1' }}
          />
      </div>
    )
    }
  }
  export default PieChart;