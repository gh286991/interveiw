import React from 'react';
import Chart from 'react-google-charts';
import moment from 'moment';

const style = {
  textAlign : 'center' ,
  // border: '5px #FFAC55 solid',
  width : '1000px',
  margin: 'auto'

};

class CohortTable extends React.Component {
    render() {
      const {cohort } =this.props;

      const data = [
        [
          { type: 'string', label: 'Custome' },
        ], 
       ]
 
      Object.keys(cohort).forEach((value) =>{
          const date = moment(Number(value)).format('YYYY-MM-DD')
          data[0].push(
            { type: 'string', label: date },
          )
      })

      const customers = Object.keys(Object.values(cohort)[0])
      customers.forEach(value =>{
          const list = [value ]
          Object.values(cohort).forEach((i) =>{
                list.push( i[value] )
          })
        data.push([...
            list
        ])
      })
   

    return(
      <div style={style}>
        <h2>Cohort Analysis</h2>
        <Chart
            chartType="Table"
            loader={<div>Loading Chart</div>}
            data={data}
            // options={{
            //     showRowNumber: true,
            // }}
            rootProps={{ 'data-testid': '1' }}
            />
       
      </div>
    )
    }
  }
  export default CohortTable;