import React from 'react';
import MainPage from './MainPage'


class Get_data extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoaded: true,
      error: null,
    };
  }

  componentDidMount() {
  
    fetch("http://127.0.0.1:8000/api/get_data")
      .then(res => res.json())
      .then((result) => {
          this.setState({
            isLoaded: false,
            data : result
          });
        },(error) => {
          this.setState({
            error: error
          });
        }
      ) 
      

    }
  

  render() {
    
    const { isLoaded ,error } = this.state;

   return (
    <div>
      {isLoaded ? "Loadding..." : <MainPage data = {this.state.data}></MainPage>}
    </div>
   )
  
}

}
export default Get_data;