import React from 'react';
import logo from './logo.svg';
import './App.css';
import Get_data from './component/Get_data'

function App() {
  return (
    <div className="App">
      <Get_data></Get_data>
    </div>
  );
}

export default App;
