from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
import datetime
import pandas as pd
import json

# Create your views here.

def get_index(request):
    
    return render(request,'index.html',locals())


def get_data(request):
    df = pd.read_csv('./data/order.csv',encoding='utf-8')
    top3 = get_top3()
    cohorts = cohort(df)

    Shippings = len(df.index)
    noShipping = int(df.loc[df["shipping"] == 0].shipping.count())

    
    result = {
        'Shippings' : Shippings ,
        'noShippings' : noShipping,
        'cohort' : cohorts,
        'top3' : top3
    }

    return JsonResponse(result)

def cohort(df):
    created_at = df['created_at']
    datelist = []
    format_str = '%Y/%m/%d'

    for  date in created_at:
        date_str = date.split()[0]
        dateData = datetime.datetime.strptime(date_str, format_str).date()
        datelist.append(dateData)
    
    df['date'] = pd.Series(datelist)

    date = df.date.drop_duplicates(keep='first', inplace=False)
    date = date.sort_values()
    
    customer = df.customer_id.drop_duplicates(keep='first', inplace=False)
    cohort = pd.DataFrame(customer)
    cohort = cohort.set_index('customer_id')

    for i in date[0:len(date)]:
        cohort[i] = 0
        fliter = (df["date"] == i)
        
        for ids in df[fliter]['customer_id']:
            
            cohort.loc[ids , i]  = cohort.loc[ids , i ] + 1

    cohorts = cohort.to_json()
    return cohorts
  
def get_top3():
    df = pd.read_csv('./data/order_item.csv',encoding='utf-8')
    orderDf = pd.read_csv('./data/order.csv',encoding='utf-8')
    
    product = df.product_name.drop_duplicates(keep='first', inplace=False)
    top = pd.DataFrame(product)

    top['count'] = 0
    top = top.set_index('product_name')

    for orderID in orderDf['order_id']:

        fliter = (df["order_id"] == orderID)

        for item in df[fliter]['product_name']:
            top.loc[item , 'count']  = top.loc[item , 'count']+ 1

    top.sort_values("count",inplace=True,ascending=False)

    itemlsit = []
    for item in top[0:4].index:
        itemlsit.append(item)

    return itemlsit
